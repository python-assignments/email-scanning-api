from flask import jsonify
from flask_restx import Resource
from werkzeug.exceptions import BadRequest

from app.database import db_session
from app.models import RulesPreferences
from app.schemas import RulesPreferenceSchema
from app import API


rules_preferences_ns = API.namespace('rules-preferences')


@rules_preferences_ns.route('/')
class RulesPreferencesList(Resource):

    def get(self):
        rules_preferences = db_session.query(RulesPreferences).all()
        schema = RulesPreferenceSchema(many=True)
        data = schema.dump(rules_preferences)
        return jsonify({'rules_preferences': data})


@rules_preferences_ns.route('/<string:key>')
class RulesPreference(Resource):

    def get(self, key):
        rules_preference = db_session.query(RulesPreferences).get(key)
        if rules_preference is None:
            raise BadRequest("Rules preference with given key doesn't exist.")
        schema = RulesPreferenceSchema(many=False)
        data = schema.dump(rules_preference)
        return jsonify(data)
