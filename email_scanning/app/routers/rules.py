from flask import jsonify
from flask_restx import Resource
from werkzeug.exceptions import BadRequest

from app.database import db_session
from app.models import Rules
from app.schemas import RuleSchema
from app import API


rules_ns = API.namespace('rules')


@rules_ns.route('/')
class RulesList(Resource):

    def get(self):
        rules = db_session.query(Rules).all()
        schema = RuleSchema(many=True)
        data = schema.dump(rules)
        return jsonify({'rules': data})


@rules_ns.route('/<string:name>/v/<int:version>/')
class Rule(Resource):

    def get(self, name, version):
        rule = db_session.query(Rules).get((name, version))
        if rule is None:
            raise BadRequest("Rule with given name and version doesn't exist.")
        schema = RuleSchema(many=False)
        data = schema.dump(rule)
        return jsonify(data)
