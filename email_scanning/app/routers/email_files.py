from flask import jsonify
from werkzeug.exceptions import BadRequest

from app.database import db_session
from app.models import EmailFiles
from app.schemas import EmailFileSchema
from app import API
from flask_restx import Resource, fields


email_files_ns = API.namespace('email-files')


@email_files_ns.route('/')
class EmailFilesList(Resource):

    def get(self):
        email_files = db_session.query(EmailFiles).all()
        schema = EmailFileSchema(many=True)
        data = schema.dump(email_files)
        return jsonify({'email_files': data})


@email_files_ns.route('/<string:email_id>')
class EmailFile(Resource):

    def get(self, email_id):
        email_file = db_session.query(EmailFiles).get(email_id)
        if email_file is None:
            raise BadRequest("Email file with given ID doesn't exist.")
        schema = EmailFileSchema(many=False)
        data = schema.dump(email_file)
        return jsonify(data)
