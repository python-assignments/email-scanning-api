import marshmallow
from app import MA


class EmailFileSchema(MA.Schema):

    class Meta:
        fields = ('email_id', 'score', 'rule_hits', 'scan_time')

    email_id = marshmallow.fields.String(required=True)
    score = marshmallow.fields.Float(required=True)
    rule_hits = marshmallow.fields.String(required=True)
    scan_time = marshmallow.fields.String(required=True)
