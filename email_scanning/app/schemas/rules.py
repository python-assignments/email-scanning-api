import marshmallow
from marshmallow_oneofschema import OneOfSchema

from app import MA
from app.models.rules import HeaderRules, BodyRules


class BasicRuleSchema(MA.Schema):

    class Meta:
        fields = ('name', 'version', 'score', 'rule_type')

    name = marshmallow.fields.String(required=True)
    version = marshmallow.fields.Integer(required=True)
    score = marshmallow.fields.Float(required=True)
    rule_type = marshmallow.fields.String(required=True)


class HeaderRuleSchema(BasicRuleSchema):

    class Meta:
        fields = ('name', 'version', 'score', 'rule_type', 'header', 'body_re')

    header = marshmallow.fields.String(required=True)
    body_re = marshmallow.fields.String(required=True)


class BodyRuleSchema(BasicRuleSchema):

    class Meta:
        fields = ('name', 'version', 'score', 'rule_type', 'body_re')

    body_re = marshmallow.fields.String(required=True)


class RuleSchema(OneOfSchema):

    type_schemas = {
            'body': BodyRuleSchema,
            'header': HeaderRuleSchema
    }

    def get_obj_type(self, obj):
        if isinstance(obj, BodyRules):
            return 'body'
        elif isinstance(obj, HeaderRules):
            return 'header'
        else:
            raise Exception("Unknown object type {type}".format(type=obj.__class__.__name__))
