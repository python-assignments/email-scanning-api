import marshmallow

from app import MA


class RulesPreferenceSchema(MA.Schema):

    class Meta:
        fields = ('key', 'value')

    key = marshmallow.fields.String(required=True)
    value = marshmallow.fields.String(required=True)
