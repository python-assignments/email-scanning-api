from sqlalchemy import Column, String

from app.database import Base


class Rules(Base):

    __tablename__ = 'rules'
    __table_args__ = {'autoload': True, 'extend_existing': True}
    rule_type = Column('rule_type')
    __mapper_args__ = {
        'polymorphic_identity': 'rule',
        'polymorphic_on': rule_type
    }


class HeaderRules(Rules):

    __tablename__ = 'header_rules'
    __table_args__ = {'autoload': True}
    __mapper_args__ = {
        'polymorphic_identity': 'header',
    }


class BodyRules(Rules):

    __tablename__ = 'body_rules'
    __table_args__ = {'autoload': True}
    __mapper_args__ = {
        'polymorphic_identity': 'body',
    }
