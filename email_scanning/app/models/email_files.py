from app.database import Base


class EmailFiles(Base):

    __tablename__ = 'email_files'
    __table_args__ = {'autoload': True}
