from app.database import Base


class RulesPreferences(Base):

    __tablename__ = 'rules_preferences'
    __table_args__ = {'autoload': True}
