from flask import Flask, Blueprint
from flask_marshmallow import Marshmallow
from flask_restx import Api

from app.database import db_session


APP = Flask(__name__)
MA = Marshmallow(APP)
BLUEPRINT = Blueprint('api', __name__, url_prefix='/api/v1')
API = Api(app=BLUEPRINT, version='1.0', title='EmailScanning API')
APP.register_blueprint(BLUEPRINT)


@APP.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


from .routers import email_files, rules, rules_preferences
